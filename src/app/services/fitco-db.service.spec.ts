import { TestBed } from '@angular/core/testing';

import { FitcoDbService } from './fitco-db.service';

describe('FitcoDbService', () => {
  let service: FitcoDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FitcoDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
