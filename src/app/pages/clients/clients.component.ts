import { Component, OnInit } from '@angular/core';
import { FitcoDbService } from '../../services/fitco-db.service';
import { AngularFireDatabase } from '@angular/fire/database';
import Swal from 'sweetalert2';
import { ClientModel } from 'src/app/models/client.model';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css'],
})
export class ClientsComponent implements OnInit {
  clients: ClientModel[] = [];
  clientSelected: any;
  loading = true;
  items: ClientModel[];

  constructor(
    private fitcoService: FitcoDbService,
    private db: AngularFireDatabase
  ) {}

  ngOnInit(): void {
    this.db
      .list('clients')
      .valueChanges()
      .subscribe((resp: ClientModel[]) => {
        this.clients = resp;
        this.loading = false;
        this.insertIdToClients(this.clients);
      });
  }

  insertIdToClients(clients: ClientModel[]) {
    this.db
      .list('clients')
      .snapshotChanges()
      .subscribe((resp: any) =>
        resp.forEach((value: any, index) => (clients[index].id = value.key))
      );
  }

  deleteClient(client: ClientModel, index: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `Delete ${client.name} !`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        this.fitcoService.deleteClient(client.id).subscribe();
        this.clients.splice(index, 1);
        Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
      }
    });
  }
}
