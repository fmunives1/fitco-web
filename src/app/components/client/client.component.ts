import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FitcoDbService } from 'src/app/services/fitco-db.service';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientModel } from 'src/app/models/client.model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
})
export class ClientComponent implements OnInit {
  client: ClientModel = new ClientModel();
  loading = true;
  constructor(
    private fitcoService: FitcoDbService,
    private routerLink: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id = this.routerLink.snapshot.paramMap.get('id');
    if (id !== 'new') {
      this.fitcoService.getClient(id).subscribe((resp: ClientModel) => {
        this.client = resp;
        this.client.id = id;
        this.loading = false;
      });
    } else {
      this.loading = false;
    }
  }

  saveInfo(form: NgForm) {
    if (form.invalid) {
      return;
    }

    if (this.client.id) {
      this.fitcoService.updateClient(this.client).subscribe(() => {
        Swal.fire({
          title: 'Client updated',
          text: `${this.client.name} has beeen updated`,
          icon: 'success',
        });
        this.router.navigate(['/clients']);
      });
    } else {
      this.fitcoService.createClient(this.client).subscribe(() => {
        Swal.fire({
          title: 'Client created',
          text: `${this.client.name} has beeen created`,
          icon: 'success',
        });
        this.router.navigate(['/clients']);
      });
    }
  }
}
